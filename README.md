# ECサイト(ポテパンキャンプ課題)
Solidusを用いたECサイトです。  
既存のECサイトへの追加機能の実装を行いました。  
# 実装機能一覧
・商品詳細ページ(https://potepanec-20200128113723.herokuapp.com/potepan/products/1 など)  
・カテゴリーページ(https://potepanec-20200128113723.herokuapp.com/potepan/categories/3 など)  
・トップページ(https://potepanec-20200128113723.herokuapp.com/potepan)
# 開発の流れ
作業ブランチからマスターブランチにプルリクエストを作成し、現役Webエンジニア3人以上からコードレビューを受けて修正し、マージ承認(2人からLGTM)をもらってからマージするという流れを繰り返すことで開発を進めました。 
# 実装のポイント
・RSpec(Request Spec、Feature Spec)でテスト記述  
・Fat Controllerを避けるため、一部メソッドをモデルで定義  
・大量のデータにも対応できるよう、Rubyでのデータ処理を減らし極力SQL側で処理  
・SQLのクエリ回数を出来るだけ少なくできるようにコントローラーを工夫
・Bootstrapによるレスポンシブ対応  
・Rubocopを使用した静的解析  
# 使用技術
・Ruby 2.5.1  
・Ruby on Rails 5.2.1  
・MySQL  
・Heroku  
・AWS(S3)  
・Docker  
・CircleCI(CI/CD)  
・Git  
・GitHub  